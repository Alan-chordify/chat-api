defmodule ChatApi.Repo.Migrations.CreateDispatches do
  use Ecto.Migration

  def change do
    create table(:dispatches) do
      add :content, :string
      add :user_id, references(:users, on_delete: :delete_all)
      add :responder_id, references(:responders, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:dispatches, [:responder_id])
  end
end
