defmodule ChatApi.Repo.Migrations.CreateMessages do
  use Ecto.Migration

  def change do
    create table(:messages) do
      add :content, :string
      add :user_id, references(:users, on_delete: :nothing)
      add :responder_id, references(:responders, on_delete: :nothing)
      add :dispatch_id, references(:dispatches, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:messages, [:user_id])
    create index(:messages, [:responder_id])
    create index(:messages, [:dispatch_id])
  end
end
