defmodule ChatApi.Repo.Migrations.CreateResponders do
  use Ecto.Migration

  def change do
    create table(:responders) do
      add :name, :string
      add :organization_id, references(:organizations, on_delete: :nothing)

      timestamps(type: :utc_datetime)
    end

    create index(:responders, [:organization_id])
  end
end
