# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     ChatApi.Repo.insert!(%ChatApi.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.
alias ChatApi.{Chat.Organization, Chat.User, Chat.Responder, Chat.Dispatch}

# Clear existing data
ChatApi.Repo.delete_all(Organization)
ChatApi.Repo.delete_all(User)
ChatApi.Repo.delete_all(Responder)
ChatApi.Repo.delete_all(Dispatch)

# Organizations
organization1 = %Organization{name: "Organization 1"}
organization2 = %Organization{name: "Organization 2"}

[organization1, organization2]
|> Enum.each(&ChatApi.Repo.insert!(&1))

# Users
user1 = %User{name: "User 1"}
user2 = %User{name: "User 2"}
user3 = %User{name: "User 3"}

[user1, user2, user3]
|> Enum.each(&ChatApi.Repo.insert!(&1))

# Responders
responder1 = %Responder{name: "Responder 1", organization_id: organization1.id}
responder2 = %Responder{name: "Responder 2", organization_id: organization2.id}

[responder1, responder2]
|> Enum.each(&ChatApi.Repo.insert!(&1))

# Dispatches
dispatch1 = %Dispatch{content: "Dispatch 1 content", responder_id: responder1.id}
dispatch2 = %Dispatch{content: "Dispatch 2 content", responder_id: responder2.id}
dispatch3 = %Dispatch{content: "Dispatch 3 content", responder_id: responder1.id}

[dispatch1, dispatch2, dispatch3]
|> Enum.each(&ChatApi.Repo.insert!(&1))

IO.puts "Seeds inserted successfully!"
