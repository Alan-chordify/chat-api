defmodule ChatApi.Chat do
  @moduledoc """
  The Chat context.
  """

  import Ecto.Query, warn: false
  alias ChatApi.Repo

  alias ChatApi.Chat.Organization

  @doc """
  Returns the list of organizations.

  ## Examples

      iex> list_organizations()
      [%Organization{}, ...]

  """
  def list_organizations do
    Repo.all(Organization)
  end

  @doc """
  Gets a single organization.

  Raises `Ecto.NoResultsError` if the Organization does not exist.

  ## Examples

      iex> get_organization!(123)
      %Organization{}

      iex> get_organization!(456)
      ** (Ecto.NoResultsError)

  """
  def get_organization!(id), do: Repo.get!(Organization, id)

  @doc """
  Creates a organization.

  ## Examples

      iex> create_organization(%{field: value})
      {:ok, %Organization{}}

      iex> create_organization(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_organization(attrs \\ %{}) do
    %Organization{}
    |> Organization.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a organization.

  ## Examples

      iex> update_organization(organization, %{field: new_value})
      {:ok, %Organization{}}

      iex> update_organization(organization, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_organization(%Organization{} = organization, attrs) do
    organization
    |> Organization.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a organization.

  ## Examples

      iex> delete_organization(organization)
      {:ok, %Organization{}}

      iex> delete_organization(organization)
      {:error, %Ecto.Changeset{}}

  """
  def delete_organization(%Organization{} = organization) do
    Repo.delete(organization)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking organization changes.

  ## Examples

      iex> change_organization(organization)
      %Ecto.Changeset{data: %Organization{}}

  """
  def change_organization(%Organization{} = organization, attrs \\ %{}) do
    Organization.changeset(organization, attrs)
  end

  alias ChatApi.Chat.User

  @doc """
  Returns the list of users.

  ## Examples

      iex> list_users()
      [%User{}, ...]

  """
  def list_users do
    Repo.all(User)
  end

  @doc """
  Gets a single user.

  Raises `Ecto.NoResultsError` if the User does not exist.

  ## Examples

      iex> get_user!(123)
      %User{}

      iex> get_user!(456)
      ** (Ecto.NoResultsError)

  """
  def get_user!(id), do: Repo.get!(User, id)

  @doc """
  Creates a user.

  ## Examples

      iex> create_user(%{field: value})
      {:ok, %User{}}

      iex> create_user(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_user(attrs \\ %{}) do
    %User{}
    |> User.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a user.

  ## Examples

      iex> update_user(user, %{field: new_value})
      {:ok, %User{}}

      iex> update_user(user, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_user(%User{} = user, attrs) do
    user
    |> User.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a user.

  ## Examples

      iex> delete_user(user)
      {:ok, %User{}}

      iex> delete_user(user)
      {:error, %Ecto.Changeset{}}

  """
  def delete_user(%User{} = user) do
    Repo.delete(user)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking user changes.

  ## Examples

      iex> change_user(user)
      %Ecto.Changeset{data: %User{}}

  """
  def change_user(%User{} = user, attrs \\ %{}) do
    User.changeset(user, attrs)
  end

  alias ChatApi.Chat.Responder

  @doc """
  Returns the list of responders.

  ## Examples

      iex> list_responders()
      [%Responder{}, ...]

  """
  def list_responders do
    Repo.all(Responder)
  end

  @doc """
  Gets a single responder.

  Raises `Ecto.NoResultsError` if the Responder does not exist.

  ## Examples

      iex> get_responder!(123)
      %Responder{}

      iex> get_responder!(456)
      ** (Ecto.NoResultsError)

  """
  def get_responder!(id), do: Repo.get!(Responder, id)

  @doc """
  Creates a responder.

  ## Examples

      iex> create_responder(%{field: value})
      {:ok, %Responder{}}

      iex> create_responder(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_responder(attrs \\ %{}) do
    %Responder{}
    |> Responder.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a responder.

  ## Examples

      iex> update_responder(responder, %{field: new_value})
      {:ok, %Responder{}}

      iex> update_responder(responder, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_responder(%Responder{} = responder, attrs) do
    responder
    |> Responder.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a responder.

  ## Examples

      iex> delete_responder(responder)
      {:ok, %Responder{}}

      iex> delete_responder(responder)
      {:error, %Ecto.Changeset{}}

  """
  def delete_responder(%Responder{} = responder) do
    Repo.delete(responder)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking responder changes.

  ## Examples

      iex> change_responder(responder)
      %Ecto.Changeset{data: %Responder{}}

  """
  def change_responder(%Responder{} = responder, attrs \\ %{}) do
    Responder.changeset(responder, attrs)
  end

  alias ChatApi.Chat.Dispatch

  @doc """
  Returns the list of dispatches.

  ## Examples

      iex> list_dispatches()
      [%Dispatch{}, ...]

  """
  def list_dispatches do
    Repo.all(Dispatch)
  end

  @doc """
  Gets a single dispatch.

  Raises `Ecto.NoResultsError` if the Dispatch does not exist.

  ## Examples

      iex> get_dispatch!(123)
      %Dispatch{}

      iex> get_dispatch!(456)
      ** (Ecto.NoResultsError)

  """
  def get_dispatch!(id), do: Repo.get!(Dispatch, id)

  @doc """
  Creates a dispatch.

  ## Examples

      iex> create_dispatch(%{field: value})
      {:ok, %Dispatch{}}

      iex> create_dispatch(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_dispatch(attrs \\ %{}) do
    %Dispatch{}
    |> Dispatch.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a dispatch.

  ## Examples

      iex> update_dispatch(dispatch, %{field: new_value})
      {:ok, %Dispatch{}}

      iex> update_dispatch(dispatch, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_dispatch(%Dispatch{} = dispatch, attrs) do
    dispatch
    |> Dispatch.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a dispatch.

  ## Examples

      iex> delete_dispatch(dispatch)
      {:ok, %Dispatch{}}

      iex> delete_dispatch(dispatch)
      {:error, %Ecto.Changeset{}}

  """
  def delete_dispatch(%Dispatch{} = dispatch) do
    Repo.delete(dispatch)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking dispatch changes.

  ## Examples

      iex> change_dispatch(dispatch)
      %Ecto.Changeset{data: %Dispatch{}}

  """
  def change_dispatch(%Dispatch{} = dispatch, attrs \\ %{}) do
    Dispatch.changeset(dispatch, attrs)
  end

  alias ChatApi.Chat.Message

  @doc """
  Returns the list of messages.

  ## Examples

      iex> list_messages()
      [%Message{}, ...]

  """
  def list_messages do
    Repo.all(Message)
  end

  @doc """
  Gets a single message.

  Raises `Ecto.NoResultsError` if the Message does not exist.

  ## Examples

      iex> get_message!(123)
      %Message{}

      iex> get_message!(456)
      ** (Ecto.NoResultsError)

  """
  def get_message!(id), do: Repo.get!(Message, id)

  @doc """
  Creates a message.

  ## Examples

      iex> create_message(%{field: value})
      {:ok, %Message{}}

      iex> create_message(%{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def create_message(attrs \\ %{}) do
    %Message{}
    |> Message.changeset(attrs)
    |> Repo.insert()
  end

  @doc """
  Updates a message.

  ## Examples

      iex> update_message(message, %{field: new_value})
      {:ok, %Message{}}

      iex> update_message(message, %{field: bad_value})
      {:error, %Ecto.Changeset{}}

  """
  def update_message(%Message{} = message, attrs) do
    message
    |> Message.changeset(attrs)
    |> Repo.update()
  end

  @doc """
  Deletes a message.

  ## Examples

      iex> delete_message(message)
      {:ok, %Message{}}

      iex> delete_message(message)
      {:error, %Ecto.Changeset{}}

  """
  def delete_message(%Message{} = message) do
    Repo.delete(message)
  end

  @doc """
  Returns an `%Ecto.Changeset{}` for tracking message changes.

  ## Examples

      iex> change_message(message)
      %Ecto.Changeset{data: %Message{}}

  """
  def change_message(%Message{} = message, attrs \\ %{}) do
    Message.changeset(message, attrs)
  end
end
