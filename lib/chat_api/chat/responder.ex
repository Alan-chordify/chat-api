defmodule ChatApi.Chat.Responder do
  use Ecto.Schema
  import Ecto.Changeset

  schema "responders" do
    field :name, :string
    belongs_to :organization, Chat.Organization
    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(responder, attrs) do
    responder
    |> cast(attrs, [:name, :organization_id]) # Ensure organization_id is permitted
    |> validate_required([:name, :organization_id])
  end
end
