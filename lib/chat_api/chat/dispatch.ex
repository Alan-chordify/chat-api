defmodule ChatApi.Chat.Dispatch do
  use Ecto.Schema
  import Ecto.Changeset

  schema "dispatches" do
    field :content, :string
    field :user_id, :id
    field :responder_id, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(dispatch, attrs) do
    dispatch
    |> cast(attrs, [:content, :user_id, :responder_id], optional: [:responder_id])
    |> validate_required([:content, :user_id])
  end
end
