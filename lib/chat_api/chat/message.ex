defmodule ChatApi.Chat.Message do
  use Ecto.Schema
  import Ecto.Changeset

  schema "messages" do
    field :content, :string
    field :user_id, :id
    field :responder_id, :id
    field :dispatch_id, :id

    timestamps(type: :utc_datetime)
  end

  @doc false
  def changeset(message, attrs) do
    message
    |> cast(attrs, [:content, :user_id, :responder_id, :dispatch_id ],  optional: [:responder_id, :user_id])
    |> validate_required([:content])
  end
end
