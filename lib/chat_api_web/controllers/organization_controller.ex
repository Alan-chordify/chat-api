defmodule ChatApiWeb.OrganizationController do
  use ChatApiWeb, :controller

  alias ChatApi.Chat
  alias ChatApi.Chat.Organization

  action_fallback ChatApiWeb.FallbackController

  def index(conn, _params) do
    organizations = Chat.list_organizations()
    render(conn, :index, organizations: organizations)
  end

  def create(conn, %{"name" => name}) do
    case Chat.create_organization(%{name: name}) do
      {:ok, organization} ->
        conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/organizations/#{organization}")
      |> render(:show, organization: organization)
    end
  end

  def show(conn, %{"id" => id}) do
    organization = Chat.get_organization!(id)
    render(conn, :show, organization: organization)
  end

  def update(conn, %{"id" => id, "organization" => organization_params}) do
    organization = Chat.get_organization!(id)

    with {:ok, %Organization{} = organization} <- Chat.update_organization(organization, organization_params) do
      render(conn, :show, organization: organization)
    end
  end

  def delete(conn, %{"id" => id}) do
    organization = Chat.get_organization!(id)

    with {:ok, %Organization{}} <- Chat.delete_organization(organization) do
      send_resp(conn, :no_content, "")
    end
  end
end
