defmodule ChatApiWeb.ResponderJSON do
  alias Hex.API.Key.Organization
  alias ChatApi.Chat.{Organization, Responder}

  @doc """
  Renders a list of responders.
  """
  def index(%{responders: responders}) do
    %{data: for(responder <- responders, do: data(responder))}
  end

  @doc """
  Renders a single responder.
  """
  def show(%{responder: responder}) do
    %{data: data(responder)}
  end

  defp data(%Responder{} = responder) do
    organization = fetch_organization(responder.organization_id)
    %{
      id: responder.id,
      name: responder.name,
      organization: %{
        id: organization.id,
        name: organization.name
      }
    }
  end

  defp fetch_organization(organization_id) do
    case ChatApi.Repo.get(Organization, organization_id) do
      nil -> %ChatApi.Chat.Organization{name: "Unknown Organization"}
      organization -> organization
    end
  end
end
