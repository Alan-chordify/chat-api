defmodule ChatApiWeb.DispatchJSON do
  alias ChatApi.Chat.{Dispatch, User, Responder}

  @doc """
  Renders a list of dispatches.
  """
  def index(%{dispatches: dispatches}) do
    %{data: for(dispatch <- dispatches, do: data(dispatch))}
  end

  @doc """
  Renders a single dispatch.
  """
  def show(%{dispatch: dispatch}) do
    %{data: data(dispatch)}
  end

  defp data(%Dispatch{} = dispatch) do
    user = fetch_user(dispatch.user_id)
    responder = fetch_responder(dispatch.responder_id)
    dispatch_data =
      %{
        id: dispatch.id,
        content: dispatch.content,
        user: %{
          id: user.id,
          name: user.name
        }
      }

    if responder do
      IO.puts("1111111111#{inspect(responder)}")
      Map.put(dispatch_data, :responder, %{
        id: responder.id,
        name: responder.name
      })
    else
      dispatch_data
    end
  end


  defp fetch_user(user_id) do
    case ChatApi.Repo.get(User, user_id) do
      nil -> %ChatApi.Chat.User{name: "Unknown User"}
      user -> user
    end
  end

  defp fetch_responder(responder_id) do
    if responder_id != nil do
      case ChatApi.Repo.get(Responder, responder_id) do
        nil -> %ChatApi.Chat.Responder{name: "Unknown Responder"}
        responder -> responder
      end
    else
      nil
    end
  end
end
