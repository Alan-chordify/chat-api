defmodule ChatApiWeb.ResponderController do
  use ChatApiWeb, :controller

  alias ChatApi.Chat
  alias ChatApi.Chat.Responder

  action_fallback ChatApiWeb.FallbackController

  def index(conn, _params) do
    responders = Chat.list_responders()
    render(conn, :index, responders: responders)
  end

  def create(conn, %{"name" => name, "organization_id" => organization_id}) do
    case organization_id do
      nil ->
        conn
        |> put_status(:bad_request)
        |> json(%{error: "Organization ID cannot be nil"})

      _ ->
        case Chat.create_responder(%{name: name, organization_id: organization_id}) do
          {:ok, responder} ->
            conn
            |> put_status(:created)
            |> put_resp_header("location", ~p"/api/responders/#{responder.id}")
            |> render(:show, responder: responder)

          {:error, _changeset} ->
            conn
            |> put_status(:unprocessable_entity)
            |> render("error.json")
        end
    end
  end


  def show(conn, %{"id" => id}) do
    responder = Chat.get_responder!(id)
    render(conn, :show, responder: responder)
  end

  def update(conn, %{"id" => id, "responder" => responder_params}) do
    responder = Chat.get_responder!(id)

    with {:ok, %Responder{} = responder} <- Chat.update_responder(responder, responder_params) do
      render(conn, :show, responder: responder)
    end
  end

  def delete(conn, %{"id" => id}) do
    responder = Chat.get_responder!(id)

    with {:ok, %Responder{}} <- Chat.delete_responder(responder) do
      send_resp(conn, :no_content, "")
    end
  end
end
