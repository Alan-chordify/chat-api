defmodule ChatApiWeb.DispatchController do
  use ChatApiWeb, :controller

  alias ChatApi.Chat
  alias ChatApi.Chat.Dispatch

  action_fallback ChatApiWeb.FallbackController

  def index(conn, _params) do
    dispatches = Chat.list_dispatches()
    render(conn, :index, dispatches: dispatches)
  end

  def create(conn, %{"dispatch" => dispatch_params}) do
    case Chat.create_dispatch(dispatch_params) do
      {:ok, dispatch} ->
        conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/dispatches/#{dispatch}")
      |> render(:show, dispatch: dispatch)
    end
  end

  def show(conn, %{"id" => id}) do
    dispatch = Chat.get_dispatch!(id)
    render(conn, :show, dispatch: dispatch)
  end

  def update(conn, %{"id" => id, "dispatch" => dispatch_params}) do
    dispatch = Chat.get_dispatch!(id)

    with {:ok, %Dispatch{} = dispatch} <- Chat.update_dispatch(dispatch, dispatch_params) do
      render(conn, :show, dispatch: dispatch)
    end
  end

  def delete(conn, %{"id" => id}) do
    dispatch = Chat.get_dispatch!(id)

    with {:ok, %Dispatch{}} <- Chat.delete_dispatch(dispatch) do
      send_resp(conn, :no_content, "")
    end
  end
end
