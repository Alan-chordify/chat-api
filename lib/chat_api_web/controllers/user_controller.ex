defmodule ChatApiWeb.UserController do
  use ChatApiWeb, :controller

  alias ChatApi.Chat
  alias ChatApi.Chat.{Dispatch, User}

  action_fallback ChatApiWeb.FallbackController

  def index(conn, _params) do
    users = Chat.list_users()
    render(conn, :index, users: users)
  end

  def create(conn, %{"name" => name}) do
    case Chat.create_user(%{name: name}) do
      {:ok, user} ->
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/users/#{user}")
      |> render(:show, user: user)
    end
  end

  def show(conn, %{"id" => id}) do
    user = Chat.get_user!(id)
    render(conn, :show, user: user)
  end

  def update(conn, %{"id" => id, "user" => user_params}) do
    user = Chat.get_user!(id)

    with {:ok, %User{} = user} <- Chat.update_user(user, user_params) do
      render(conn, :show, user: user)
    end
  end

  def delete(conn, %{"id" => id}) do
    user = Chat.get_user!(id)

    with {:ok, %User{}} <- Chat.delete_user(user) do
      send_resp(conn, :no_content, "")
    end
  end
end
