defmodule ChatApiWeb.MessageController do
  import Ecto.Query, only: [from: 2]
  use ChatApiWeb, :controller

  alias ChatApi.Chat.Message
  alias ChatApi.Repo

  action_fallback ChatApiWeb.FallbackController

  def index(conn, %{"id" => id}) do
    id = String.to_integer(id)
    IO.puts(id)

    query = from(m in Message,
                 where: m.dispatch_id == ^id)
    messages = Repo.all(query)
    render(conn, :index, messages: messages)
  end

  def create(conn, %{"message" => message_params}) do
    with {:ok, %Message{} = message} <- Chat.create_message(message_params) do
      conn
      |> put_status(:created)
      |> put_resp_header("location", ~p"/api/messages/#{message}")
      |> render(:show, message: message)
    end
  end

  def show(conn, %{"id" => id}) do
    message = Chat.get_message!(id)
    render(conn, :show, message: message)
  end

  def update(conn, %{"id" => id, "message" => message_params}) do
    message = Chat.get_message!(id)

    with {:ok, %Message{} = message} <- Chat.update_message(message, message_params) do
      render(conn, :show, message: message)
    end
  end

  def delete(conn, %{"id" => id}) do
    message = Chat.get_message!(id)

    with {:ok, %Message{}} <- Chat.delete_message(message) do
      send_resp(conn, :no_content, "")
    end
  end
end
