defmodule ChatApiWeb.MessageJSON do
  alias ChatApi.Chat.Message

  @doc """
  Renders a list of messages.
  """
  def index(%{messages: messages}) do
    %{data: for(message <- messages, do: data(message))}
  end

  @doc """
  Renders a single message.
  """
  def show(%{message: message}) do
    %{data: data(message)}
  end

  defp data(%Message{} = message) do
    %{
      id: message.id,
      content: message.content,
      user: message.user_id,
      responder_id: message.responder_id
    }
  end
end
