defmodule ChatApiWeb.Router do
  use ChatApiWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", ChatApiWeb do
    pipe_through :api
    get "/users", UserController, :index
    get "/users/:id", UserController, :show
    post "/users", UserController, :create
    get "/organizations", OrganizationController, :index
    post "/organizations", OrganizationController, :create
    get "/dispatches", DispatchController, :index
    post "/dispatches", DispatchController, :create
    patch "/dispatches/:id", DispatchController, :update
    get "/responders", ResponderController, :index
    get "/messages/:id", MessageController, :index
    post "/messages", MessageController, :create
    post "/responders", ResponderController, :create
  end

  # Enable LiveDashboard and Swoosh mailbox preview in development
  if Application.compile_env(:chat_api, :dev_routes) do
    # If you want to use the LiveDashboard in production, you should put
    # it behind authentication and allow only admins to access it.
    # If your application does not have an admins-only section yet,
    # you can use Plug.BasicAuth to set up some basic authentication
    # as long as you are also using SSL (which you should anyway).
    import Phoenix.LiveDashboard.Router

    scope "/dev" do
      pipe_through [:fetch_session, :protect_from_forgery]

      live_dashboard "/dashboard", metrics: ChatApiWeb.Telemetry
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end
end
