defmodule ChatApiWeb.ResponderControllerTest do
  use ChatApiWeb.ConnCase

  import ChatApi.ChatFixtures

  alias ChatApi.Chat.Responder

  @create_attrs %{
    name: "some name"
  }
  @update_attrs %{
    name: "some updated name"
  }
  @invalid_attrs %{name: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all responders", %{conn: conn} do
      conn = get(conn, ~p"/api/responders")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create responder" do
    test "renders responder when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/api/responders", responder: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/responders/#{id}")

      assert %{
               "id" => ^id,
               "name" => "some name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/api/responders", responder: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update responder" do
    setup [:create_responder]

    test "renders responder when data is valid", %{conn: conn, responder: %Responder{id: id} = responder} do
      conn = put(conn, ~p"/api/responders/#{responder}", responder: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/responders/#{id}")

      assert %{
               "id" => ^id,
               "name" => "some updated name"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, responder: responder} do
      conn = put(conn, ~p"/api/responders/#{responder}", responder: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete responder" do
    setup [:create_responder]

    test "deletes chosen responder", %{conn: conn, responder: responder} do
      conn = delete(conn, ~p"/api/responders/#{responder}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/responders/#{responder}")
      end
    end
  end

  defp create_responder(_) do
    responder = responder_fixture()
    %{responder: responder}
  end
end
