defmodule ChatApiWeb.DispatchControllerTest do
  use ChatApiWeb.ConnCase

  import ChatApi.ChatFixtures

  alias ChatApi.Chat.Dispatch

  @create_attrs %{
    content: "some content"
  }
  @update_attrs %{
    content: "some updated content"
  }
  @invalid_attrs %{content: nil}

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all dispatches", %{conn: conn} do
      conn = get(conn, ~p"/api/dispatches")
      assert json_response(conn, 200)["data"] == []
    end
  end

  describe "create dispatch" do
    test "renders dispatch when data is valid", %{conn: conn} do
      conn = post(conn, ~p"/api/dispatches", dispatch: @create_attrs)
      assert %{"id" => id} = json_response(conn, 201)["data"]

      conn = get(conn, ~p"/api/dispatches/#{id}")

      assert %{
               "id" => ^id,
               "content" => "some content"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn} do
      conn = post(conn, ~p"/api/dispatches", dispatch: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "update dispatch" do
    setup [:create_dispatch]

    test "renders dispatch when data is valid", %{conn: conn, dispatch: %Dispatch{id: id} = dispatch} do
      conn = put(conn, ~p"/api/dispatches/#{dispatch}", dispatch: @update_attrs)
      assert %{"id" => ^id} = json_response(conn, 200)["data"]

      conn = get(conn, ~p"/api/dispatches/#{id}")

      assert %{
               "id" => ^id,
               "content" => "some updated content"
             } = json_response(conn, 200)["data"]
    end

    test "renders errors when data is invalid", %{conn: conn, dispatch: dispatch} do
      conn = put(conn, ~p"/api/dispatches/#{dispatch}", dispatch: @invalid_attrs)
      assert json_response(conn, 422)["errors"] != %{}
    end
  end

  describe "delete dispatch" do
    setup [:create_dispatch]

    test "deletes chosen dispatch", %{conn: conn, dispatch: dispatch} do
      conn = delete(conn, ~p"/api/dispatches/#{dispatch}")
      assert response(conn, 204)

      assert_error_sent 404, fn ->
        get(conn, ~p"/api/dispatches/#{dispatch}")
      end
    end
  end

  defp create_dispatch(_) do
    dispatch = dispatch_fixture()
    %{dispatch: dispatch}
  end
end
