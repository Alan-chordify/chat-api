defmodule ChatApi.ChatTest do
  use ChatApi.DataCase

  alias ChatApi.Chat

  describe "organizations" do
    alias ChatApi.Chat.Organization

    import ChatApi.ChatFixtures

    @invalid_attrs %{name: nil}

    test "list_organizations/0 returns all organizations" do
      organization = organization_fixture()
      assert Chat.list_organizations() == [organization]
    end

    test "get_organization!/1 returns the organization with given id" do
      organization = organization_fixture()
      assert Chat.get_organization!(organization.id) == organization
    end

    test "create_organization/1 with valid data creates a organization" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Organization{} = organization} = Chat.create_organization(valid_attrs)
      assert organization.name == "some name"
    end

    test "create_organization/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chat.create_organization(@invalid_attrs)
    end

    test "update_organization/2 with valid data updates the organization" do
      organization = organization_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Organization{} = organization} = Chat.update_organization(organization, update_attrs)
      assert organization.name == "some updated name"
    end

    test "update_organization/2 with invalid data returns error changeset" do
      organization = organization_fixture()
      assert {:error, %Ecto.Changeset{}} = Chat.update_organization(organization, @invalid_attrs)
      assert organization == Chat.get_organization!(organization.id)
    end

    test "delete_organization/1 deletes the organization" do
      organization = organization_fixture()
      assert {:ok, %Organization{}} = Chat.delete_organization(organization)
      assert_raise Ecto.NoResultsError, fn -> Chat.get_organization!(organization.id) end
    end

    test "change_organization/1 returns a organization changeset" do
      organization = organization_fixture()
      assert %Ecto.Changeset{} = Chat.change_organization(organization)
    end
  end

  describe "users" do
    alias ChatApi.Chat.User

    import ChatApi.ChatFixtures

    @invalid_attrs %{name: nil}

    test "list_users/0 returns all users" do
      user = user_fixture()
      assert Chat.list_users() == [user]
    end

    test "get_user!/1 returns the user with given id" do
      user = user_fixture()
      assert Chat.get_user!(user.id) == user
    end

    test "create_user/1 with valid data creates a user" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %User{} = user} = Chat.create_user(valid_attrs)
      assert user.name == "some name"
    end

    test "create_user/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chat.create_user(@invalid_attrs)
    end

    test "update_user/2 with valid data updates the user" do
      user = user_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %User{} = user} = Chat.update_user(user, update_attrs)
      assert user.name == "some updated name"
    end

    test "update_user/2 with invalid data returns error changeset" do
      user = user_fixture()
      assert {:error, %Ecto.Changeset{}} = Chat.update_user(user, @invalid_attrs)
      assert user == Chat.get_user!(user.id)
    end

    test "delete_user/1 deletes the user" do
      user = user_fixture()
      assert {:ok, %User{}} = Chat.delete_user(user)
      assert_raise Ecto.NoResultsError, fn -> Chat.get_user!(user.id) end
    end

    test "change_user/1 returns a user changeset" do
      user = user_fixture()
      assert %Ecto.Changeset{} = Chat.change_user(user)
    end
  end

  describe "responders" do
    alias ChatApi.Chat.Responder

    import ChatApi.ChatFixtures

    @invalid_attrs %{name: nil}

    test "list_responders/0 returns all responders" do
      responder = responder_fixture()
      assert Chat.list_responders() == [responder]
    end

    test "get_responder!/1 returns the responder with given id" do
      responder = responder_fixture()
      assert Chat.get_responder!(responder.id) == responder
    end

    test "create_responder/1 with valid data creates a responder" do
      valid_attrs = %{name: "some name"}

      assert {:ok, %Responder{} = responder} = Chat.create_responder(valid_attrs)
      assert responder.name == "some name"
    end

    test "create_responder/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chat.create_responder(@invalid_attrs)
    end

    test "update_responder/2 with valid data updates the responder" do
      responder = responder_fixture()
      update_attrs = %{name: "some updated name"}

      assert {:ok, %Responder{} = responder} = Chat.update_responder(responder, update_attrs)
      assert responder.name == "some updated name"
    end

    test "update_responder/2 with invalid data returns error changeset" do
      responder = responder_fixture()
      assert {:error, %Ecto.Changeset{}} = Chat.update_responder(responder, @invalid_attrs)
      assert responder == Chat.get_responder!(responder.id)
    end

    test "delete_responder/1 deletes the responder" do
      responder = responder_fixture()
      assert {:ok, %Responder{}} = Chat.delete_responder(responder)
      assert_raise Ecto.NoResultsError, fn -> Chat.get_responder!(responder.id) end
    end

    test "change_responder/1 returns a responder changeset" do
      responder = responder_fixture()
      assert %Ecto.Changeset{} = Chat.change_responder(responder)
    end
  end

  describe "dispatches" do
    alias ChatApi.Chat.Dispatch

    import ChatApi.ChatFixtures

    @invalid_attrs %{content: nil}

    test "list_dispatches/0 returns all dispatches" do
      dispatch = dispatch_fixture()
      assert Chat.list_dispatches() == [dispatch]
    end

    test "get_dispatch!/1 returns the dispatch with given id" do
      dispatch = dispatch_fixture()
      assert Chat.get_dispatch!(dispatch.id) == dispatch
    end

    test "create_dispatch/1 with valid data creates a dispatch" do
      valid_attrs = %{content: "some content"}

      assert {:ok, %Dispatch{} = dispatch} = Chat.create_dispatch(valid_attrs)
      assert dispatch.content == "some content"
    end

    test "create_dispatch/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chat.create_dispatch(@invalid_attrs)
    end

    test "update_dispatch/2 with valid data updates the dispatch" do
      dispatch = dispatch_fixture()
      update_attrs = %{content: "some updated content"}

      assert {:ok, %Dispatch{} = dispatch} = Chat.update_dispatch(dispatch, update_attrs)
      assert dispatch.content == "some updated content"
    end

    test "update_dispatch/2 with invalid data returns error changeset" do
      dispatch = dispatch_fixture()
      assert {:error, %Ecto.Changeset{}} = Chat.update_dispatch(dispatch, @invalid_attrs)
      assert dispatch == Chat.get_dispatch!(dispatch.id)
    end

    test "delete_dispatch/1 deletes the dispatch" do
      dispatch = dispatch_fixture()
      assert {:ok, %Dispatch{}} = Chat.delete_dispatch(dispatch)
      assert_raise Ecto.NoResultsError, fn -> Chat.get_dispatch!(dispatch.id) end
    end

    test "change_dispatch/1 returns a dispatch changeset" do
      dispatch = dispatch_fixture()
      assert %Ecto.Changeset{} = Chat.change_dispatch(dispatch)
    end
  end

  describe "messages" do
    alias ChatApi.Chat.Message

    import ChatApi.ChatFixtures

    @invalid_attrs %{content: nil}

    test "list_messages/0 returns all messages" do
      message = message_fixture()
      assert Chat.list_messages() == [message]
    end

    test "get_message!/1 returns the message with given id" do
      message = message_fixture()
      assert Chat.get_message!(message.id) == message
    end

    test "create_message/1 with valid data creates a message" do
      valid_attrs = %{content: "some content"}

      assert {:ok, %Message{} = message} = Chat.create_message(valid_attrs)
      assert message.content == "some content"
    end

    test "create_message/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Chat.create_message(@invalid_attrs)
    end

    test "update_message/2 with valid data updates the message" do
      message = message_fixture()
      update_attrs = %{content: "some updated content"}

      assert {:ok, %Message{} = message} = Chat.update_message(message, update_attrs)
      assert message.content == "some updated content"
    end

    test "update_message/2 with invalid data returns error changeset" do
      message = message_fixture()
      assert {:error, %Ecto.Changeset{}} = Chat.update_message(message, @invalid_attrs)
      assert message == Chat.get_message!(message.id)
    end

    test "delete_message/1 deletes the message" do
      message = message_fixture()
      assert {:ok, %Message{}} = Chat.delete_message(message)
      assert_raise Ecto.NoResultsError, fn -> Chat.get_message!(message.id) end
    end

    test "change_message/1 returns a message changeset" do
      message = message_fixture()
      assert %Ecto.Changeset{} = Chat.change_message(message)
    end
  end
end
