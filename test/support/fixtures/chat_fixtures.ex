defmodule ChatApi.ChatFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `ChatApi.Chat` context.
  """

  @doc """
  Generate a organization.
  """
  def organization_fixture(attrs \\ %{}) do
    {:ok, organization} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> ChatApi.Chat.create_organization()

    organization
  end

  @doc """
  Generate a user.
  """
  def user_fixture(attrs \\ %{}) do
    {:ok, user} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> ChatApi.Chat.create_user()

    user
  end

  @doc """
  Generate a responder.
  """
  def responder_fixture(attrs \\ %{}) do
    {:ok, responder} =
      attrs
      |> Enum.into(%{
        name: "some name"
      })
      |> ChatApi.Chat.create_responder()

    responder
  end

  @doc """
  Generate a dispatch.
  """
  def dispatch_fixture(attrs \\ %{}) do
    {:ok, dispatch} =
      attrs
      |> Enum.into(%{
        content: "some content"
      })
      |> ChatApi.Chat.create_dispatch()

    dispatch
  end

  @doc """
  Generate a message.
  """
  def message_fixture(attrs \\ %{}) do
    {:ok, message} =
      attrs
      |> Enum.into(%{
        content: "some content"
      })
      |> ChatApi.Chat.create_message()

    message
  end
end
